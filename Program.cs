﻿using System;
using System.Threading.Tasks;

namespace Opposite
{
    class Program
    {
        static async Task Main(string[] args)
        {
            int a = 5;//2
            int b = 6;//1
            int c = a + b;//4
            int d = a;//3
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);
             Resturant();
            await MyResturant();
        }
        public static void Eaters1(string arrive, int time)
        {
            //Task.Delay(3000).Wait();
            Console.WriteLine($"Eater {arrive} arrive's and  food is ready {time} mins at this time");
        }
        public static void Resturant()
        {
            Eaters1("1", 90);
            Eaters1("2", 30);
            Eaters1("3", 10);
            Eaters1("4", 50);
            Eaters1("5", 5);
        }

        public static async Task Eaters(string arrive, int time)
        {
            await Task.CompletedTask;
            Console.WriteLine($"Eater {arrive}'s, food is ready at {time} mins");
        }
        public static async Task MyResturant()
        {
            await Eaters("1", 90);
           await Eaters("2", 30);
           await Eaters("3", 10);
           await Eaters("4", 50);
           await Eaters("5", 5);
        }



    }   }   
        
    
